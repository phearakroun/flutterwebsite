import 'package:flutter/material.dart';

class DrawerPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: [
        UserAccountsDrawerHeader(
            currentAccountPicture: CircleAvatar(
              backgroundColor: Colors.teal,
              child: Icon(
                Icons.public,
                color: Colors.red,
              ),
            ),
            accountName: Text("Theam Developer"),
            accountEmail: Text("theam@theam.com")),
      ],
    ));
  }
}
