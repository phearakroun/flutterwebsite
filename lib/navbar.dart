import 'package:appweb/navbar/desktopnavbar.dart';
import 'package:appweb/navbar/mobilenavbar.dart';
import 'package:flutter/material.dart';

class NavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constrains) {
      if (constrains.maxWidth > 800) {
        return DesktopNavbar();
      } else {
        return MobileNavbar();
      }
    });
  }
}
