import 'package:flutter/material.dart';

class DesktopNavbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            "Web Application Dev",
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.white, fontSize: 30),
          ),
          Row(
            children: [
              Text(
                "Home",
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(width: 30),
              Text(
                "About Us",
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(width: 30),
              Text(
                "Portfolio",
                style: TextStyle(color: Colors.white),
              ),
              SizedBox(width: 30),
              MaterialButton(
                color: Colors.pink,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                ),
                onPressed: () {
                  print("Get Start");
                },
                child: Text(
                  "Get Start",
                  style: TextStyle(color: Colors.white),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
