import 'package:flutter/material.dart';

class MobileNavbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        children: [
          Text(
            "Web Application Dev",
            style: TextStyle(
                fontWeight: FontWeight.bold, color: Colors.white, fontSize: 30),
          ),
          Padding(
              padding: EdgeInsets.all(20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Home",
                    style: TextStyle(color: Colors.white),
                  ),
                  SizedBox(width: 30),
                  Text(
                    "About Us",
                    style: TextStyle(color: Colors.white),
                  ),
                  SizedBox(width: 30),
                  Text(
                    "Portfolio",
                    style: TextStyle(color: Colors.white),
                  ),
                  SizedBox(width: 30),
                  MaterialButton(
                    color: Colors.pink,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                    ),
                    onPressed: () {
                      print("Get Start");
                    },
                    child: Text(
                      "Get Start",
                      style: TextStyle(color: Colors.white),
                    ),
                  )
                ],
              ))
        ],
      ),
    );
  }
}
